extern crate esparse_derive;

use core::ops::Add;
use std::convert::TryInto;
use std::mem::size_of;
pub use esparse_derive::*;

pub type Tag = [u8; 4];

#[derive(Debug, Clone, Copy)]
pub struct Span
{
    pub start: usize,
    pub end: usize,
}

impl Span{
    pub fn len(&self) -> Option<usize>{
        
        self.end.checked_sub(self.start)
    }

    pub fn relative(self, start: usize, end: usize) -> Span
    {
        let start = self.start + start;
        let end = self.start + end;
        Span{start, end}
    }
}

impl Add<usize> for Span{
    type Output = Span;

    fn add(self, rhs: usize) -> Self{
        Span{
            start: self.start + rhs,
            ..self
        }
    }
}

#[derive(Debug)]
pub struct WrongTag
{
    expected: Tag,
    found: Tag
}

impl WrongTag{
    pub fn new(expected: Tag, found: Tag) -> Self{
        Self{
            expected,
            found
        }
    }
}

#[derive(Debug)]
pub struct Error
{
    pub span: Span,
    pub line: u32,
    pub file: String,
    pub kind: ErrorKind
}

#[derive(Debug)]
pub enum ErrorKind
{
    Dummy,
    WrongTag(WrongTag),
    EOS,
    WrongSize,
}



pub type Result<T> = std::result::Result<T, Error>;

pub trait FromBytes : Sized{
    fn from_bytes(input: &[u8], span: Span, tag: Option<Tag>) -> Result<(Self, usize)>;
}

macro_rules! fromBytesPrimitive {
    ($name:ty) => {
        impl FromBytes for $name{
            fn from_bytes(input: &[u8], span: Span, _tag:Option<Tag>) -> Result<(Self, usize)>{
                let size = size_of::<$name>();

                if span.len().unwrap_or_default() < size
                {
                    return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) });
                }
                Ok((<$name>::from_le_bytes(input[span.start..span.start+ size].try_into().expect("slice with incorrect length")), size))
            }
        }
    };
}

fromBytesPrimitive!(i8);
fromBytesPrimitive!(u8);

fromBytesPrimitive!(i16);
fromBytesPrimitive!(u16);

fromBytesPrimitive!(i32);
fromBytesPrimitive!(u32);
fromBytesPrimitive!(f32);

fromBytesPrimitive!(i64);
fromBytesPrimitive!(u64);
fromBytesPrimitive!(f64);


impl FromBytes for Tag{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        if span.len().unwrap_or_default() < 4
        {
            return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) });
        }
        let val = [input[span.start + 0], input[span.start + 1], input[span.start + 2], input[span.start + 3] ];
        if let Some(tag) = tag{
            if val != tag
            {
                return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::WrongTag(WrongTag{expected: tag, found:val})});
            }
        }
        Ok((val, 4))
    }
}

#[derive(Debug)]
pub struct ZString{
    value: String
}

impl FromBytes for ZString
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        if span.len().unwrap_or_default() == 0
        {
            return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) });
        }
        let tmp = String::from_utf8_lossy(&input[span.start..span.end-1]).into_owned();

        Ok((ZString{value: tmp} , span.end - span.start))
    }
}

pub type LString = ZString;

#[derive(Debug)]
pub struct WString{
    value: String
}

impl FromBytes for WString
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        if input.len() == 0
        {
            return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) });
        }
        
        let (length, local_offset) = u16::from_bytes(input, span.relative(0,2), tag)?;
        let length = usize::from(length);
        dbg!(&length);
        let tmp = String::from_utf8_lossy(&input[span.start +local_offset ..span.start +local_offset+length]).into_owned();

        Ok((WString{value: tmp} , usize::from(length+ local_offset)))
    }
}

#[derive(Debug)]
pub struct WZString{
    value: String
}

impl FromBytes for WZString
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        if input.len() == 0
        {
            return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) });
        }
        
        let (length, local_offset) = u16::from_bytes(input, Span{end: span.start +2, ..span}, tag)?;
        let length = usize::from(length);
        let tmp = String::from_utf8_lossy(&input[span.start +local_offset ..span.start +local_offset+length-1]).into_owned();

        Ok((WZString{value: tmp} , usize::from(length+ local_offset)))
    }
}

impl FromBytes for String
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        if input.len() == 0
        {
            return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) });
        }
        
        let tmp = String::from_utf8_lossy(&input[span.start ..span.end]).into_owned();

        Ok((tmp, span.len().unwrap_or_default()))
    }
}

impl<T> FromBytes for Option<T> where T:FromBytes
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        if span.len().unwrap_or_default() == 0
        {
            return Ok((None, 0));
        }

        let inner = T::from_bytes(input, span, tag);

        match inner{
            Ok((val, len)) => Ok((Some(val), len)),
            Err(error) => match error.kind {
                ErrorKind::WrongTag(_) => Ok((None, 0)),
                ErrorKind::EOS => Ok((None, 0)),
                _ => Err(error) 
            }
        }
    }
}

impl<T> FromBytes for Vec<T> where T:FromBytes
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        let mut result = vec![];
        let mut read_bytes = 0;

        loop 
        {
            // dbg!(&input[read_bytes..]);
            // dbg!(input[read_bytes..].len());
            match T::from_bytes(&input, span + read_bytes, tag)
            {
                Ok((inner, bytes)) => {
                    result.push(inner);
                    read_bytes += bytes;
                },
                Err(error) => 
                    match error.kind {
                        ErrorKind::EOS => break,
                        ErrorKind::WrongTag(_) => break,
                        _ => return (Err(error))
                    }
            }
        }

        Ok((result, read_bytes))
    }
}