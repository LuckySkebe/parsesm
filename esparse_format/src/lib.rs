#[cfg(test)]
mod tests {

    #[test]
    fn should_read_to_end() {
        use crate::data::tes4::*;
        use crate::data::*;
        use esparse::{FromBytes, Span};
        let buffer :&[u8] = include_bytes!("../../plugins/SmashTestPlugin01.esp");
        let (data, read) = SkyrimSEFile::from_bytes(buffer, Span{start:0, end:buffer.len()}, None).unwrap();

        dbg!(&data);
        assert_eq!(read, buffer.len());
    }
}

pub mod prelude;
mod data;