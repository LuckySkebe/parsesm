pub mod tes4;
use esparse::{*};

// enum File{
//     Tes4(TES4)
// }

#[derive(FromBytes, Debug)]
pub struct RecordMetadata{
    pub tag : Tag,
    pub length: u32,
    flags: u32,
    id: u32,
    version_control_info: u32,
    version: u16,
    unknown: u16,
}

#[derive(Debug)]
pub struct Record<TData : FromBytes>{
    metadata: RecordMetadata,
    data: TData
}


impl<TData> FromBytes for Record<TData>
    where TData: FromBytes
{

    fn from_bytes(input: &[u8], span: Span, tag: Option<Tag>) -> Result<(Self, usize)>
    {
        if input.len() == 0
        {
            return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::EOS });
        }
        let (metadata, metadata_length) = RecordMetadata::from_bytes(input, span, tag)?;

        if let Some(tag) = tag
        {
            if tag != metadata.tag
            {
                return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::WrongTag(WrongTag::new(tag, metadata.tag))});
            }
        }

        let span = span.relative(metadata_length, metadata_length + metadata.length as usize);

        

        let (data, data_length) = TData::from_bytes(&input, span , None)?;


        Ok((Self{
            metadata,
            data
        }, metadata_length + data_length))
    }
}

#[derive(FromBytes, Debug)]
pub struct SubRecordMetadata{
    pub tag : Tag,
    pub length: u16
}

#[derive(Debug)]
pub struct SubRecord<TData : FromBytes>{
    metadata: SubRecordMetadata,
    data: TData
}

impl<TData> FromBytes for SubRecord<TData>
    where TData: FromBytes
{

    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>
    {
        if input.len() == 0
        {
            return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) });
        }
        let (metadata, metadata_length) = SubRecordMetadata::from_bytes(input, span, tag)?;

        if let Some(tag) = tag
        {
            if tag != metadata.tag
            {
                return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::WrongTag(WrongTag::new(tag, metadata.tag))});
            }
        }

        let (data, data_length) = TData::from_bytes(&input, span.relative(metadata_length, metadata_length + usize::from( metadata.length)) , None)?;

        Ok((Self{
            metadata,
            data
        }, metadata_length + data_length))
    }
}

#[derive(FromBytes, Debug)]
pub struct GroupMetadata{
    pub tag : Tag,
    pub length: u32,
    pub label: Tag,
    pub groupType: u32,
    pub versionControlInfo: u32,
    _unkonwn: u32,
}

#[derive(Debug)]
pub struct Group<TData : FromBytes>{
    metadata: GroupMetadata,
    data: Vec<TData>
}

impl<TData> FromBytes for Group<TData>
    where TData: FromBytes
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>
    {
        if input.len() == 0
        {
            return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::EOS });
        }
        let (metadata, metadata_length) = GroupMetadata::from_bytes(input, span, tag)?;

        let (data, data_length) = Vec::<TData>::from_bytes(&input, span.relative(metadata_length as usize, metadata.length as usize), None)?;

        Ok((Self{
            metadata,
            data
        }, metadata_length + data_length))
    }
}

#[derive(FromBytes, Debug)]
pub struct FieldMetadata{
    pub tag : Tag,
    pub length: u16,
}

#[derive(Debug)]
pub struct Field<TData : FromBytes>{
    metadata: FieldMetadata,
    data: TData
}

impl<TData> FromBytes for Field<TData>
    where TData: FromBytes
{

    fn from_bytes(input: &[u8], span:Span, tag:Option<Tag>) -> Result<(Self, usize)>
    {
        if input.len() == 0
        {
            return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::EOS });
        }
        let (metadata, metadata_length) = FieldMetadata::from_bytes(input, span, tag)?;

        if let Some(tag) = tag
        {
            if tag != metadata.tag
            {
                return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::WrongTag(WrongTag::new(tag, metadata.tag))});
            }
        }

        let (data, data_length) = TData::from_bytes(&input, span.relative(metadata_length, metadata_length + usize::from(metadata.length)), None)?;

        Ok((Self{
            metadata,
            data
        }, metadata_length + data_length))
    }
}

type FormId = u32;