use crate::data::{FormId, SubRecord, Record, Group, Field, GroupMetadata};
use esparse::{*};

#[derive(FromBytes, Debug)]
pub struct Header
{
    version: f32,
    num_records: i32,
    next_object_id: u32
}

#[derive(FromBytes, Debug)]
pub struct Master
{
    master_file: SubRecord::<ZString>,
    data: SubRecord::<u64>
}

#[derive(FromBytes, Debug)]
pub struct TES4
{
    #[Tag = b"HEDR"]
    header: SubRecord::<Header>,
    #[Tag= b"CNAM"]
    author: Option::<SubRecord::<ZString>>,
    #[Tag= b"SNAM"]
    description: Option::<SubRecord::<ZString>>,
    #[Tag= b"MAST"]
    master_files: Vec::<Master>,
    #[Tag = b"ONAM"]
    overrides: Option::<SubRecord::<Vec<FormId>>>,
    #[Tag = b"INTV"]
    internal_version: Option::<SubRecord::<u32>>,
    #[Tag = b"INCC"]
    incc: Option::<SubRecord::<u32>>
}

#[derive(FromBytes, Debug)]
pub struct Keyword {
    #[Tag = b"EDID"]
    editor_id: Field::<ZString>,

    #[Tag = b"CNAM"]
    color: Option::<Field::<u32>>
}

#[derive(Debug)]
pub struct ScriptData{

}

impl FromBytes for ScriptData{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        unimplemented!()
    }
}

#[derive(Debug)]
pub struct Model{

}

impl FromBytes for Model{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        unimplemented!()
    }
}

#[derive(Debug)]
pub struct Unimplemented{

}

impl FromBytes for Unimplemented{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        dbg!(tag);
        unimplemented!()
    }
}

#[derive(FromBytes, Debug)]
pub struct ObjectBounds{
    x1: i16,
    y1: i16,
    z1: i16,
    x2: i16,
    y2: i16,
    z2: i16,
}

#[derive(FromBytes, Debug)]
pub struct Activator{ //TODO: Implement missing fields
    #[Tag = b"EDID"]
    editor_id: Field::<ZString>,
    #[Tag = b"VMAD"]
    script_data: Option::<Field::<ScriptData>>,
    #[Tag = b"OBND"]
    object_bounds: Field::<ObjectBounds>
}

#[derive(FromBytes, Debug)]
pub struct BodyTemplate{
    body_part: u32,
    flags: u8,
    junk_data_1: u8,
    junk_data_2: u8,
    junk_data_3: u8,
    skill: u32,
}

pub enum ModelData{
    Version40(ModelData40)
}

#[derive(Debug)]
pub struct Unknown4{
    unknown1: u32,
    texture_type: ZString,
    unknown2: u32,
}

impl FromBytes for Unknown4
{
    fn from_bytes(input: &[u8], span: Span, tag:Option<Tag>) -> Result<(Self, usize)>{
        match span.len().unwrap_or_default()
        {
            0 => return Err(Error{ span, line: line!(), kind: ErrorKind::EOS, file: String::from(file!()) }),
            x if x<12 => return Err(Error{ span, line: line!(), kind: ErrorKind::WrongSize, file: String::from(file!()) }),
            _ => (),
        }
        
        let (unknown1, _) = u32::from_bytes(input, span.relative(0,4), None)?;
        let (texture_type, _) = ZString::from_bytes(input, span.relative(4,8), None)?;
        let (unknown2, _) = u32::from_bytes(input, span.relative(8,12), None)?;

        Ok((Self{unknown1, texture_type, unknown2}, 12))
    }
}

#[derive(Debug)]
pub struct ModelData40{
    count: u32,
    unknown4count: Option::<u32>,
    unknown5count:Option::<u32>,
    unknown3: Vec<u32>,
    unknown4: Vec<Unknown4>,
    unknown5: Vec<u32>,
}

impl FromBytes for ModelData40
{
    fn from_bytes(input: &[u8], span: Span, _tag:Option<Tag>) -> Result<(Self, usize)>{
        
        let mut offset = 0;
        let (count, len) = u32::from_bytes(input, span.relative(0,4), None)?;
        
        

        offset += len;
        let unknown4count = if count >= 1 {
            let (value, len) = u32::from_bytes(input, span.relative(offset,offset+4), None)?;
            offset += len;
            
            Some(value)
        }else {
            None
        };
        let unknown5count = if count >= 2 {
            let (value, len) = u32::from_bytes(input, span.relative(offset,offset+4), None)?;
            offset += len;
            Some(value)
        }else {
            None
        };

        let unknown3len= 4* (count as usize -2);

        let (unknown3, len) = Vec::<u32>::from_bytes(input, span.relative(offset, offset + unknown3len), None)?;
        
        offset += len;
        let unknown4len = (unknown4count.unwrap_or_default() * 12) as usize;
        let (unknown4, len) = Vec::<Unknown4>::from_bytes(input, span.relative(offset, offset + unknown4len), None)?;
        offset += len;
        let unknown5len = (unknown5count.unwrap_or_default() * 4) as usize;
        let (unknown5, _) = Vec::<u32>::from_bytes(input, span.relative(offset, offset + unknown5len), None)?;
        offset += unknown5len;

        let ret = Self{
            count,
            unknown4count,
            unknown5count,
            unknown3,
            unknown4,
            unknown5,
        };

        Ok((ret, offset))
    }
}


#[derive(FromBytes, Debug)]
pub struct Data{
    base_value: u32,
    weight: f32,
}

#[derive(FromBytes, Debug)]
pub struct Armor
{
    #[Tag = b"EDID"]
    editor_id: Field::<ZString>,
    #[Tag = b"VMAD"]
    script_data: Option::<Field::<ScriptData>>,
    #[Tag = b"OBND"]
    object_bounds: Field::<ObjectBounds>,
    #[Tag = b"FULL"]
    full_name: Option::<Field::<LString>>, //TODO: Implement LString
    #[Tag = b"EITM"]
    enchantment : Option::<Field::<FormId>>,
    #[Tag = b"EATM"]
    enchantment_amount: Option::<Field::<u16>>,
    #[Tag = b"MODL"]
    model: Option::<Field::<Model>>,
    #[Tag = b"MOD2"]
    male_model: Field::<ZString>,
    #[Tag = b"MO2T"]
    male_model_data: Option::<Field::<ModelData40>>, //TODO Handle type 38
    #[Tag = b"MO2S"]
    male_alternate_textures: Option::<Field::<Unimplemented>>,
    #[Tag = b"ICON"]
    male_inventory_image: Option::<Field::<ZString>>,
    #[Tag = b"MICO"]
    male_message_image: Option::<Field::<ZString>>,
    #[Tag = b"MOD4"]
    female_model: Option::<Field::<ZString>>,
    #[Tag = b"MO4T"]
    female_model_data: Option::<Field::<ModelData40>>,
    #[Tag = b"MO4S"]
    female_alternate_textures: Option::<Field::<Unimplemented>>,
    #[Tag = b"ICO2"]
    female_inventory_image: Option::<Field::<ZString>>,
    #[Tag = b"MIC2"]
    female_message_image: Option::<Field::<ZString>>,
    #[Tag = b"BODT"]
    body_template: Field::<BodyTemplate>,
    #[Tag = b"BOD2"]
    body_template2: Option::<Field::<u64>>,
    #[Tag = b"DEST"]
    destruction_data: Option::<Field::<Unimplemented>>,
    #[Tag = b"YNAM"]
    pickup_sound: Option::<Field::<FormId>>,
    #[Tag = b"ZNAM"]
    drop_sound: Option::<Field::<FormId>>,
    #[Tag = b"BMCT"]
    ragdoll : Option::<Field::<WString>>,
    #[Tag = b"ETYP"]
    equip_slot: Option::<Field::<FormId>>,
    #[Tag = b"BIDS"]
    bash_impact_data_set: Option::<Field::<FormId>>,
    #[Tag = b"BAMT"]
    bash_material: Option::<Field::<FormId>>,
    #[Tag = b"RNAM"]
    race: Field::<FormId>,
    #[Tag = b"KSIZ"]
    keyword_count: Option::<Field::<u32>>,
    #[Tag = b"KWDA"]
    keywords: Option::<Field::<Vec::<FormId>>>,
    #[Tag = b"DESC"]
    description: Field::<LString>,
    #[Tag = b"MODL"]
    armatures: Field::<Vec::<FormId>>,
    #[Tag = b"DATA"]
    data: Field::<Data>,
    #[Tag = b"DNAM"]
    armor_rating: Field::<u32>,
    #[Tag = b"TNAM"]
    template: Option::<Field::<FormId>>,
}

#[derive(Debug)]
pub struct SkyrimSEGroups{
    keywords: Option<Group::<Record::<Keyword>>>,
    armors: Option<Group<Record<Armor>>>
}

impl FromBytes for SkyrimSEGroups {
    fn from_bytes(input: &[u8], span: Span, tag: std::option::Option<[u8; 4]>) -> std::result::Result<(Self, usize), esparse::Error> {
        let mut keywords = None;
        let mut armors = None;

        let (metadata, length) = GroupMetadata::from_bytes(input, span, tag)?;
        let mut offset = 0;

        match &metadata.label
        {
            b"KYWD" => {
                let (group, length) = Group::<Record::<Keyword>>::from_bytes(&input, span+ offset, tag)?;
                keywords = Some(group);
                offset += length
            },
            b"ARMO" => {
                let (group, length) = Group::<Record::<Armor>>::from_bytes(&input, span+ offset, tag)?;
                armors = Some(group);
                offset += length
            },
            _ => {}
        }

        Ok((
            SkyrimSEGroups{
                keywords,
                armors
            },
            offset
        ))
    }
}

#[derive(FromBytes, Debug)]
pub struct SkyrimSEFile{
    #[Tag = b"TES4"]
    tes4: Record::<TES4>,
    #[Tag = b"GRUP"]
    groups: SkyrimSEGroups
}