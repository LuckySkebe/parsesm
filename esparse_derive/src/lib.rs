extern crate proc_macro;
extern crate proc_macro2;

use crate::proc_macro::{TokenStream};
use quote::quote;
use syn;
use syn::{parse_macro_input, DeriveInput, Meta};

fn impl_from_bytes(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;

    match &ast.data{
        syn::Data::Struct(struct_data) => {
            let fields = &struct_data.fields;

            match &fields{
                syn::Fields::Named(fields) => {
                    let vars = fields.named.iter().map(|field| {
                        let name = &field.ident;
                        let ty = &field.ty;

                        let expected_tag = &field.attrs.iter().find(|attr| attr.path.is_ident("Tag")).map(|attr| attr.parse_meta());

                        let tmp = if let Some(Ok(Meta::NameValue(expected_tag))) = expected_tag{
                            match expected_tag.lit
                            {
                                syn::Lit::ByteStr(ref b)=> Some(b.clone()),
                                _ => None
                            }
                        } else {
                            None
                        };

                        let tag = if let Some(tag) = tmp {
                            quote!{
                                Some(*#tag)
                            }
                        }else {
                            quote!{
                                None
                            }
                        };

                        quote!{
                            let expected_tag = #tag;

                            let (#name, read_bytes) = #ty::from_bytes(&input, span +sum_read_bytes, expected_tag)?;
                            sum_read_bytes += read_bytes;
                        }
                    });

                    let names = fields.named.iter().map(|field| {
                        &field.ident
                    });

                    let gen = quote! {
                        impl FromBytes for #name {
                            fn from_bytes(input: &[u8], span: Span, expected_tag: Option<Tag>) -> Result<(Self, usize)> {
                                if span.len().unwrap_or_default() == 0
                                {
                                    return Err(Error{ span, line: line!(), file: String::from(file!()), kind: ErrorKind::EOS });
                                }

                                let mut sum_read_bytes = 0;

                                #(#vars)*

                                Ok((#name {
                                    #(#names),*
                                },sum_read_bytes))
                            }
                        }
                    };
                    gen.into()
                }
                syn::Fields::Unnamed(fields) => {
                    unimplemented!()
                }
                syn::Fields::Unit => {
                    unimplemented!()
                }
            }
        }
        syn::Data::Enum(_) => unimplemented!(),
        syn::Data::Union(_) => panic!("This macro does not support unions")
    }
}

#[proc_macro_derive(FromBytes, attributes(Tag))]
pub fn from_bytes_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = parse_macro_input!(input as DeriveInput);

    // Build the trait implementation
    impl_from_bytes(&ast)
}

// #[proc_macro_derive(Record, attributes(Tag))]
// pub fn record_macro_derive(input: TokenStream) -> TokenStream {
//     // Construct a representation of Rust code as a syntax tree
//     // that we can manipulate
//     let ast = parse_macro_input!(input as DeriveInput);

//     // Build the trait implementation
//     impl_record(&ast)
// }

// #[proc_macro_derive(Field, attributes(Tag))]
// pub fn field_macro_derive(input: TokenStream) -> TokenStream {
//     // Construct a representation of Rust code as a syntax tree
//     // that we can manipulate
//     let ast = parse_macro_input!(input as DeriveInput);

//     // Build the trait implementation
//     impl_field(&ast)
// }